﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStatus : MonoBehaviour
{
    // config parameters
    [Range(0.0f, 2.0f)] [SerializeField] float gameSpeed = 1.0f;
    [SerializeField] int pointPerBlockDestroyed = 10;
    [SerializeField] Text scoreText;
    [SerializeField] bool isAutoPlayEnabled;

    // state variables
    int currentScore;

    private void Awake()
    {
        // 注意，是FindObject"s"OfType
        int gameStatusCount = FindObjectsOfType<GameStatus>().Length;

        if(gameStatusCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        UpdateScoreText();
    }

    public void AddScore()
    {
        currentScore += pointPerBlockDestroyed;
        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        scoreText.text = currentScore.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = gameSpeed;
    }

    public void ResetGame()
    {
        Destroy(gameObject);
    }

    public bool IsAutoPlayEnabled()
    {
        return isAutoPlayEnabled;
    }
}
