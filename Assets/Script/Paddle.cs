﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    // config params
    [SerializeField] float screenWidthUnits = 16f;
    [SerializeField] float screenMinUnits = 1f;
    [SerializeField] float screenMaxUnits = 15f;

    // cached references
    GameStatus gameStatus;
    Ball ball;

    // Use this for initialization
    void Start () {
        gameStatus = FindObjectOfType<GameStatus>();
        ball = FindObjectOfType<Ball>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector2 paddlePos = new Vector2(GetPosX(), transform.position.y);

        transform.position = paddlePos;
	}

    float GetPosX()
    {
        float posX;
        if (!gameStatus.IsAutoPlayEnabled())
        {
            posX = Input.mousePosition.x / Screen.width * screenWidthUnits;
        }
        else
        {
            posX = ball.gameObject.transform.position.x;
        }

        return Mathf.Clamp(posX, screenMinUnits, screenMaxUnits);
    }
}
