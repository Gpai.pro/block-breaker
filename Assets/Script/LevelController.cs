﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {
    
    int blocks ;

    // cached reference
    SceneLoader sceneLoader;

    private void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
    }

    public void AddBlock()
    {
        blocks ++;
    }

    public void RemoveBlock()
    {
        blocks--;

        if (blocks <= 0)
            sceneLoader.LoadNextScene();
    }
}
