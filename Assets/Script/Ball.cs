﻿using UnityEngine;

public class Ball : MonoBehaviour {

    // config params
    [SerializeField] Paddle paddle;
    [SerializeField] float ballVelocityX = 0f;
    [SerializeField] float ballVelocityY = 15f;
    [SerializeField] AudioClip[] ballSounds;
    [SerializeField] float randomFactor = 0.2f;

    // cached component references
    AudioSource audio;
    Rigidbody2D rigidbody;

    // state
    Vector2 paddleToBallOffset;    
    bool hasStarted;

	// Use this for initialization
	void Start () {
        paddleToBallOffset = transform.position - paddle.transform.position;

        hasStarted = false;

        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!hasStarted)
        {
            MoveWithPaddle();
            LaunchOnMouseClick();
        }
    }

    private void LaunchOnMouseClick()
    {
        if(Input.GetMouseButtonDown(0))
        {
            hasStarted = true;
            rigidbody.velocity = new Vector2(ballVelocityX, ballVelocityY);
        }        
    }

    private void MoveWithPaddle()
    {
        Vector2 paddlePos = new Vector2(paddle.transform.position.x, paddle.transform.position.y);

        transform.position = paddlePos + paddleToBallOffset;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 velocityTweak = new Vector2(Random.Range(0f, randomFactor), Random.Range(0f, randomFactor));
        
        if (hasStarted)
        {
            AudioClip clip = ballSounds[UnityEngine.Random.Range(0, ballSounds.Length)];
            audio.PlayOneShot(clip);

            rigidbody.velocity += velocityTweak;
        }
    }
}
