﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

    // config params
    [SerializeField] AudioClip breakSound;
    [SerializeField] GameObject particle;
    [SerializeField] Sprite[] sprites;

    // cached reference
    LevelController levelController;
    GameStatus gameStatus;
    SpriteRenderer spriteRender;

    // state variables
    int timesHit = 0;

    private void Start()
    {
        gameStatus = FindObjectOfType<GameStatus>();

        levelController = FindObjectOfType<LevelController>();
        if (gameObject.tag == "Breakable")
        {
            levelController.AddBlock();
        }

        spriteRender = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameObject.tag == "Breakable")
        {
            HandleHitBlock();
        }
    }

    private void HandleHitBlock()
    {
        ++timesHit;
        if (timesHit == sprites.Length + 1)
        {
            DestroyBlock();
        }
        else
        {
            int index = timesHit - 1;
            if (sprites[index] != null)
            {
                spriteRender.sprite = sprites[index];
            }
            else
            {
                Debug.LogError("missing the sprite : " + gameObject.name);
            }
        }
    }

    private void DestroyBlock()
    {
        PlayParticle();
        levelController.RemoveBlock();
        gameStatus.AddScore();
        AudioSource.PlayClipAtPoint(breakSound, Camera.main.transform.position, 1);

        Destroy(gameObject);
    }

    private void PlayParticle()
    {
        GameObject obj = Instantiate(particle, transform.position, transform.rotation);
        Destroy(obj, 1.5f);
    }
}
